<?php

/*  Bisnis Logic

    User yang login sebagai pasien dapat melakukan pendaftaran periksa dokter
    secara online, dengan ketentuan :
    1. Pasien dapat memilih tanggal dan dokter
    2. Jika dokter yang dipilih pada tanggal itu tidak praktek, 
       tampilkkan error "dokter tidak praktek"
    3. Jika kuota ( jumlah pasien maksimal ) yang dapat dilayani dokter 
       sudah terpenuhi, tampilkan error "pasien sudah penuh"
    4. Jika 2 syarat dipenuhi, proses pendaftaran pasien dan pasien
       mendapatkan nomor antrian dan perkiraan jam dilayani
       ( setiap pasien dilayani 10 menit -> jam awal praktek dokter + jumlah pasien*10)
    
    User yang login sebagai dokter, akan menampilkan pasien yang diperiksa hari ini
    dan dapat menginput / mengubah jadwal dokter

    Admin dapat mengakses semua route
*/

/* 
    Penjelasan Route Auth
    Login   :   untuk login dan mendapatkan token
                ( berdasar noHP dan Password )
    Logout  :   untuk logout
    Register:   mendaftarkan user baru  (untuk pasien)              
 */

Route::namespace('Auth')->group(function() {
    Route::post('login','LoginController');
    Route::post('logout','LogoutController');
    Route::post('register','RegisterController');
});

/* 
    Penjelasan route dokter ( dilakukan dengan role dokter dan admin)
    dokter          :   melakukan input/lihat/edit/hapus data dokter
    dokter/periksa  :   dokter melihat semua pasien yang sudah mendaftar
                        pada tanggal tertentu

    Penjelasan route jadwal ( dilakukan dengan role dokter dan admin)
    jadwal          :   melakukan input/lihat/edit/hapus jadwal dokter
                        ( khusus jadwal lihat dapat juga dilihat role pasien)

    Penjelasan Route pasien ( dilakukan dengan role pasien dan admin)
    pasien          :   melakukan input/lihat/edit/hapus data pasien

    Penjelasan Route daftar ( dilakukan dengan role pasien dan admin)
    daftar          :   melakukan input/lihat/edit/hapus data pendaftaran pasien
         

 */
 
Route::namespace('Klinik')->middleware(['auth:api','admin'])->group(function() {
    
    Route::post('dokter/input','DokterController@store')->name('dokter input');   
    Route::get('dokter/lihat','DokterController@index')->name('dokter lihat');   
    Route::patch('dokter/edit/{id}','DokterController@update')->name('dokter edit');  
    Route::delete('dokter/hapus/{id}','DokterController@destroy')->name('dokter hapus');  
    
    Route::get('dokter/periksa/{id}','DokterController@index_pasien')->name('dokter periksa'); 

    Route::post('jadwal/input','JadwalController@store')->name('jadwal input');   
    Route::get('jadwal/lihat', 'JadwalController@index')->name('jadwal lihat');
    Route::patch('jadwal/edit/{id}', 'JadwalController@update')->name('jadwal edit');
    Route::delete('jadwal/hapus/{id}', 'JadwalController@destroy')->name('jadwal hapus');   

    Route::post('pasien/input','PasienController@store')->name('pasien input');        
    Route::get('pasien/lihat/{id}','PasienController@show')->name('pasien lihat');  
    Route::get('pasien/semua','PasienController@index')->name('pasien semua'); 
    Route::patch('pasien/edit/{id}','PasienController@update')->name('pasien edit');
    Route::delete('pasien/hapus/{id}', 'PasienController@destroy')->name('pasien hapus');   

    // tidak tersedia route utk edit daftar, karena pasien tidak diijinkan mengedit pendaftarannya
    Route::post('daftar/input','DaftarController@store')->name('daftar input'); 
    Route::get('daftar/lihat/{id}','DaftarController@show')->name('daftar lihat');   
    Route::get('daftar/semua','DaftarController@index')->name('daftar semua');   
    Route::delete('daftar/hapus/{id}', 'DaftarController@destroy')->name('daftar hapus');   
    Route::get('daftar/pasien/{id}','DaftarController@indexPasien')->name('daftar pasien');  
});
