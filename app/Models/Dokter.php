<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dokter extends Model
{
    protected $table = 'dokter';
    protected $guarded = [];

    public function user() {
        return $this->belongsTo('App\User','user_id');
    }

    public function spesialis()
    {
        return $this->belongsTo('App\Models\Spesialis', 'spesialis_id');
    }
}
