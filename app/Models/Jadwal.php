<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model
{
    protected $table = 'jadwal';
    protected $guarded = [];
   
    public function dokter() {
        return $this->belongsTo('App\Models\Dokter','dokter_id');
    }
  

}
