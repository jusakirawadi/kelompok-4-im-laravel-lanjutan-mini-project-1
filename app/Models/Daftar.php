<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Daftar extends Model
{
    protected $table = 'daftar';
    protected $guarded = [];

    public function getRouteKeyName(){
        return 'id';
    }

    public function user() {
        return $this->belongsTo('App\User','user_id');
    }

    public function dokter() {
        return $this->belongsTo('App\Models\Dokter','dokter_id');
    }
    
    public function jadwal() {
        return $this->belongsTo('App\Models\Jadwal','jadwal_id');
    }

}
