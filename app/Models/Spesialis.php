<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Spesialis extends Model
{
    protected $table = 'spesialis';
    protected $guarded = [];

    public function dokters()
    {
        return $this->hasMany('App\Models\Dokter');
    }
}
