<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';
    protected $guarded = [];

    public function routes() {
        return $this->belongtoMany('App\Route','role_route','role_id','route_id')
    }
}
