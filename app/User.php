<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Route;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'noHP', 'email', 'password', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function daftar() {
        return $this->hasMany('App\Models\Daftar','user_id');
    }

    public function pasien() {
        return $this->hasOne('App\Models\Pasien','user_id');
    }

    public function dokter() {
        return $this->hasOne('App\Models\Dokter','user_id');
    }

    public function cekroute($nama_route) {
        
        $route_id = Route::where('nama',$nama_route)->first()->id;
        $role_id = $this->role_id;
        
        $cek = false;
        $role_route = Role_Route::where('role_id',$role_id)->
                                  where('route_id',$route_id)->first();
        
        if ($role_route) {
            $cek = true;
        };

        return $cek;
    }

}
