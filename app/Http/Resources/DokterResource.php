<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DokterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'nama' => $this->nama,
            'alamat' => $this->alamat,
            'gender' => $this->gender,
            'spesialis' => $this->spesialis->nama
        ];

    }

    public function with($request)
    {
        return ['status' => 'Succes'];
    }
}
