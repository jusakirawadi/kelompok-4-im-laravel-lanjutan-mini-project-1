<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DaftarResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'tanggal' => $this->tanggal,
            'noantrian' => $this->noantrian,
            'status' => $this->status,
            'jadawal_id' => $this->jadwal_id,
            'dokter_id' => $this->jadwal_id,
            'pasien_id' => $this->jadwal_id,
            'user_id'=>$this->user_id,
            'nama_dokter'=>$this->dokter->nama,
            'nama_pasien'=>$this->pasien->nama
        ];
    }

    public function with($request)
    {
        return ['status' => 'Succes'];
    }
}
