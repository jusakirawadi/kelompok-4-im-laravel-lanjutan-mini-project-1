<?php

namespace App\Http\Requests\Klinik;

use Illuminate\Foundation\Http\FormRequest;

class DokterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama'   => ['required', 'alpha_num', 'max:50'],
            'alamat' => ['required', 'max:100'],
            'gender' => ['required'],
            'spesialis_id' => ['required']
        ];
    }
}
