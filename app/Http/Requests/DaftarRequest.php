<?php

namespace App\Http\Requests;

use App\Models\Daftar;
use App\Models\Jadwal;
use Illuminate\Foundation\Http\FormRequest;

class DaftarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tanggal' => ['required'],
            'dokter_id' => ['required']
        ];
    }
}
