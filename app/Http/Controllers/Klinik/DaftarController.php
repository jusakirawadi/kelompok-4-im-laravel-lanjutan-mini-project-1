<?php

namespace App\Http\Controllers\Klinik;

use App\Http\Controllers\Controller;
use App\Http\Requests\DaftarRequest;
use App\Http\Resources\DaftarResource;
use App\Models\Daftar;
use App\Models\Dokter;
use App\Models\Jadwal;
use App\Models\Pasien;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DaftarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $daftars = Daftar::get();
        return DaftarResource::collection($daftars);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DaftarRequest $request)
    {
        $dokter_id = request('dokter_id');
        $tanggal = request('tanggal');    
            
        $hari = date('w',strtotime($tanggal));
        $jadwal =   Jadwal::where('dokter_id',$dokter_id)->
                    where('hari',$hari)->first();

        if(!$jadwal ){
            return response()->json('Dokter Tdak praktek',404);
        }      

        $jmlpasien = Daftar::where('dokter_id',$dokter_id)->
                             where('tanggal', $tanggal)->count();

        $kuota = $jadwal->kuota;
               
        if($jmlpasien >= $kuota ){
            return response()->json('Pasien Sudah Penuh',403);
        }
       
        $user_id = Auth()->user()->id;
        $pasien = Pasien::where('user_id',$user_id)->first();
        if(!$pasien ){
            return response()->json('Anda Belum Melengkapi Identitas Diri !',404);
        }      

        $daftar = Daftar::where('dokter_id',$dokter_id)->
                          where('tanggal', $tanggal)->
                          where('user_id',$user_id)->first();
        if($daftar){
            return response()->json('Anda sudah mendaftar di hari ini !',403);
        }      
                

        $noantrian = $jmlpasien +1;

        $daftars=auth()->user()->daftar()->create([
            'tanggal' => request('tanggal'),
            'noantrian' => ($noantrian),
            'status' => request('status'),
            'jadwal_id' => $jadwal->id,
            'dokter_id' => request('dokter_id'),
            'pasien_id' => $pasien->id ,
            'status' => '0'
        ]);
        return $daftars;
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Daftar $daftar)
    {
        return new DaftarResource($daftar);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DaftarRequest $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $daftar = Daftar::findOrFail($id);
        $daftar ->delete();
        return response()->json('Pendaftaran Batal', 200);
    }

    public function daftarStore(){
        $pasien_id = uth()->user()->pasien()->id;
        $dokter_id = request('dokter_id');
        $tanggal = request('tanggal');
        $jmlpasien = Daftar::where('dokter_id',$dokter_id)->
                             where('tanggal', $tanggal)->count();
        $noantrian = $jmlpasien +1;
        return [
            'tanggal' => request('tanggal'),
            'noantrian' => ($noantrian),
            'status' => request('status'),
            'jadwal_id' => request('jadwal_id'),
            'dokter_id' => request('dokter_id'),
            'pasien_id' => $pasien_id 
        ];
    }
}
