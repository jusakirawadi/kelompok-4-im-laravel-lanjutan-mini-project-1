<?php

namespace App\Http\Controllers\Klinik;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Klinik\JadwalRequest;
use App\Http\Resources\JadwalResource;
use App\Models\Jadwal;

class JadwalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jadwal = Jadwal::get();
        return JadwalResource::collection($jadwal);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JadwalRequest $request)
    {
        $jadwals = Jadwal::create($this->jadwalStore());
        return new JadwalResource($jadwals);        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(JadwalRequest $request, $id)
    {
        $jadwal = Jadwal::findOrFail($id);
        $jadwal->update($this->jadwalStore());

        return $jadwal;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jadwal = Jadwal::findOrFail($id);
        $jadwal->delete();

        return response()->json('Jadwal telah dihapus', 200);
    }

    public function jadwalStore()
    {
        return [
            'hari'        => request('hari'),
            'jam_mulai'   => request('jam_mulai'),
            'jam_selesai' => request('jam_selesai'),
            'kuota'       => request('kuota'),
            'dokter_id'   => request('dokter_id'),
        ];
    }
}
