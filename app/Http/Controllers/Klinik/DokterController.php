<?php

namespace App\Http\Controllers\Klinik;

use App\Http\Controllers\Controller;
use App\Http\Requests\Klinik\DokterRequest;
use App\Http\Resources\DokterResource;
use App\Models\Daftar;
use App\Models\Dokter;
use Illuminate\Support\Facades\Auth;

class DokterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dokter = Dokter::get();
        return DokterResource::collection($dokter);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DokterRequest $request)
    {       
        $dokters = auth()->user()->dokter()->create($this->dokterStore());
        return new DokterResource($dokters);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DokterRequest $request, $id)
    {
        $dokter = Dokter::findOrFail($id);
        $dokter->update($this->dokterStore());
        return $dokter;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dokter = Dokter::findOrFail($id);
        $dokter->delete();

        return response()->json('Dokter telah dihapus', 200);
    }

    public function index_pasien($id)
    {
        $dokter_id = Auth()->user()->dokter->id;

        $daftar = Daftar::where('dokter_id', $dokter_id)->
                          whereDate('tanggal', '=', $id)->get();
        return $daftar;
    }

    public function dokterStore()
    {
        return [
            'nama'   => request('nama'),
            'alamat' => request('alamat'),
            'gender' => request('gender'),
            'spesialis_id' => request('spesialis_id')
        ];
    }
}
