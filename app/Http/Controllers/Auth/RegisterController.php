<?php

namespace App\Http\Controllers\Auth;
use App\user;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Auth\RegisterRequest;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegisterRequest $request)
    {       
        user::create([
            'name' => request('name'),
            'noHP' => request('noHP'),
            'email' => request('email'),
            'role_id' => request('role_id'),
            'password' => bcrypt(request('password'))
        ]);

        return response('Thank you, you are registered !');
    }
}
