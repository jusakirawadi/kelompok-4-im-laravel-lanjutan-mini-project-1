<?php

namespace App\Http\Middleware;

use Closure;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $nama_route = \Route::current()->getname();
        $user = \Auth::user();

        if ($user) {
            if ($user->cekroute($nama_route)) {
                return $next($request);
            }
        }
         return abort(403);
    }
}
