<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role_Route extends Model
{
    protected $table = 'role__route';
    protected $guarded = [];
}
