<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoktersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dokter', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama',50);
            $table->string('alamat',100);
            $table->string('gender',1);
            $table->unsignedBigInteger('user_id');    
            $table->unsignedBigInteger('spesialis_id');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');                       
            $table->foreign('spesialis_id')->references('id')->on('spesialis'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dokters');
    }
}
