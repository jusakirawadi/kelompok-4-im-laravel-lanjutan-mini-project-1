<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDaftarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daftar', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('tanggal');
            $table->integer('noantrian');
            $table->boolean('status');
            $table->unsignedBigInteger('jadwal_id');  
            $table->unsignedBigInteger('dokter_id');  
            $table->unsignedBigInteger('pasien_id');  
            $table->unsignedBigInteger('user_id');  
            $table->timestamps();

            $table->foreign('jadwal_id')->references('id')->on('jadwal'); 
            $table->foreign('dokter_id')->references('id')->on('dokter');
            $table->foreign('pasien_id')->references('id')->on('pasien');
            $table->foreign('user_id')->references('id')->on('users'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daftars');
    }
}
